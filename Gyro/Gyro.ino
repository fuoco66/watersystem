// brown gnd

// orange 6
// brown w 5
// orange w 4
// blue w 3

#include "MPU9250.h"

#define SDA_PIN 4
#define SCL_PIN 5

MPU9250 mpu;

void setup() {
	Serial.begin(115200);
	Wire.begin(SDA_PIN, SCL_PIN);
	delay(2000);

	// MPU9250Setting setting;
	// setting.accel_fs_sel = ACCEL_FS_SEL::A16G;
	// setting.gyro_fs_sel = GYRO_FS_SEL::G2000DPS;
	// setting.mag_output_bits = MAG_OUTPUT_BITS::M16BITS;
	// setting.fifo_sample_rate = FIFO_SAMPLE_RATE::SMPL_200HZ;
	// setting.gyro_fchoice = 0x03;
	// setting.gyro_dlpf_cfg = GYRO_DLPF_CFG::DLPF_41HZ;
	// setting.accel_fchoice = 0x01;
	// setting.accel_dlpf_cfg = ACCEL_DLPF_CFG::DLPF_45HZ;

	if (!mpu.setup(0x68)) {  // change to your own address
		while (1) {
			Serial.println("MPU connection failed. Please check your connection with `connection_check` example.");
			delay(5000);
		}
	}

	// calibrate anytime you want to
	Serial.println("Accel Gyro calibration will start in 5sec.");
	Serial.println("Please leave the device still on the flat plane.");
	pinMode(0, INPUT);
	pinMode(2, OUTPUT);
	mpu.verbose(true);
	delay(2000);

	mpu.calibrateAccelGyro();
	print_calibration();
	mpu.verbose(false);
}

void loop() {
	
	static uint32_t prev_ms = millis();
	if (millis() > prev_ms + 250) {
		
		int val = digitalRead(0);   // read the input pin
  	digitalWrite(2, val);
		
		if (mpu.update()) {		
			print_roll_pitch_yaw();
			prev_ms = millis();
		}
	}

}

void print_roll_pitch_yaw() {
	Serial.print("Yaw: ");
	Serial.println(mpu.getYaw(), 2);
  
	Serial.print("X: ");
	Serial.println(mpu.getGyroX(), 2);

	Serial.print("Euler_X: ");
	Serial.println(mpu.getEulerX(), 2);

	Serial.print("Euler_X: ");
	Serial.println(mpu.getQuaternionX(), 2);

	Serial.println("---------------");

}

void print_calibration() {
    Serial.println("< calibration parameters >");
    // Serial.println("accel bias [g]: ");
    // Serial.print(mpu.getAccBiasX() * 1000.f / (float)MPU9250::CALIB_ACCEL_SENSITIVITY);
    // Serial.print(", ");
    // Serial.print(mpu.getAccBiasY() * 1000.f / (float)MPU9250::CALIB_ACCEL_SENSITIVITY);
    // Serial.print(", ");
    // Serial.print(mpu.getAccBiasZ() * 1000.f / (float)MPU9250::CALIB_ACCEL_SENSITIVITY);
    // Serial.println();
    Serial.println("gyro bias [deg/s]: ");
    Serial.print(mpu.getGyroBiasX() / (float)MPU9250::CALIB_GYRO_SENSITIVITY);
    Serial.print(", ");
    Serial.print(mpu.getGyroBiasY() / (float)MPU9250::CALIB_GYRO_SENSITIVITY);
    Serial.print(", ");
    Serial.print(mpu.getGyroBiasZ() / (float)MPU9250::CALIB_GYRO_SENSITIVITY);
    Serial.println();

		// 0.05, 0.12, -0.91

}
