/*

    Ultrasonic sensor Pins:
        VCC: +5VDC
        Trig : Trigger (INPUT) - Pin11
        Echo: Echo (OUTPUT) - Pin 12
        GND: GND
 */
#include <Wire.h>
#include <Ultrasonic.h>
#define I2C_SLAVE_ADDRESS 0x08 // Address of the slave

#define TRIGG_PIN 12
#define ECHO_PIN 13
#define DIST_OFFSET 0

Ultrasonic ultrasonic(12, 13);

int distance;
unsigned long lastCapture = 0; 
bool ditch = true;

char command;

enum {
  CMD_ID = 1,
  CMD_READ  = 2,
};

void setup() {
  //Serial Port begin
  Serial.begin (115200);

  // makes first reading
  distance = ultrasonic.read();
  Serial.print("First Distance in CM: ");
  Serial.println(distance);
  lastCapture = millis();
  delay(1000);


  // Start the I2C Bus as Slave on address 9
  Wire.begin(I2C_SLAVE_ADDRESS); 
  
  // Attach a function to trigger when something is received.
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  
}

void receiveEvent (int howMany){
  ditch = true;

  command = Wire.read ();  // remember command for when we get request
  // Serial.println("rec");

} // end of receiveEvent


void requestEvent() {

  ditch = true;

  switch (command) {
    case CMD_ID:      Wire.write (0x55); break;   // send our ID 
    case CMD_READ:
      // Serial.println("req");

      byte SlaveSend = distance;
      Wire.write(SlaveSend);
      break;
    
  }  // end of switch
}

void loop() {

  // reads every seconds
  if((millis() - lastCapture) < 100){
    return;
  }

  // uint8_t ResArray[3];

  // uint8_t i = 0;

  // while (i < 3){
  //   int tmp = ultrasonic.read();

  //   if(i == 0 || tmp == ResArray[i-1]){
  //     ResArray[i] = tmp;
  //     i++;
  //     continue;  
  //   }

  // }

  // int tmpDistance = round((ResArray[1] + ResArray[2] + ResArray[3]) /3 ); 

  int tmpDistance = ultrasonic.read(); 

  // if interrupt breaks the capture, the result is discarted. Next reading will be immediate
  if(ditch == true){
    Serial.println("ditched");
    lastCapture = millis();
    ditch = false;
    return;
  }

  // corrects
  distance = tmpDistance + DIST_OFFSET;
  Serial.print("Distance in CM: ");
  Serial.println(distance);
  lastCapture = millis();
}


    
