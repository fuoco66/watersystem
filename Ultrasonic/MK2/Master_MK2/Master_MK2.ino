//Code for the Arduino Master
// #include <Wire.h>
// #include "AdvWire.h"
#include <I2C_Anything.h>

#define SLAVE_ADDRESS 0x08
  
enum {
    CMD_ID = 1,
    CMD_READ = 2,
    CMD_READ_D8 = 3
    };

void sendCommand (const byte cmd, const int responseSize){
  Wire.beginTransmission(SLAVE_ADDRESS);
  Wire.write(cmd);
  Wire.endTransmission();
  
  // Wire.requestFrom (SLAVE_ADDRESS, responseSize);  

  if (Wire.requestFrom (SLAVE_ADDRESS, 1) == 0) {
    Serial.println( "Failed" );
  }
  else{
    Serial.println( "Success" );
  }
  
}  // end of sendCommand
  

void setup() {

 Serial.begin(115200); // start serial for output
 Serial.println( "Setup Success" );

  // advWireInterface.begin(CNC_ADDRESS, receiveEvent);
  Wire.begin ();
  
  sendCommand(CMD_ID, 1);
  
  if (Wire.available ()){
    Serial.print("Slave is ID: ");
    Serial.println(Wire.read (), DEC);
  }
  else{
    Serial.println ("No response to ID request");
  }  // end of setup

}
  

void loop() {
  
  // Wire.requestFrom(SLAVE_ADDRESS,2);
  // byte MasterReceive = Wire.read();     
  // Serial.println( "Recieved: " );
  // Serial.println(MasterReceive);

  sendCommand (CMD_READ, 1);
  
  if (Wire.available ()){
    Serial.print("Recieved: ");
    Serial.println(Wire.read (), DEC);
  }
  else{
    Serial.println ("No response");
  }  // end of setup


  delay(1000);
}
