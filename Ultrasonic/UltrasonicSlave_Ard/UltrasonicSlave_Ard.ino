/*

    Ultrasonic sensor Pins:
        VCC: +5VDC
        Trig : Trigger (INPUT) - Pin11
        Echo: Echo (OUTPUT) - Pin 12
        GND: GND
 */
#include <Wire.h>
// #include <I2C_Anything.h>
#include <Ultrasonic.h>
#define I2C_SLAVE_ADDRESS 0x08 // Address of the slave

#define TRIGG_PIN 12
#define ECHO_PIN 13
const byte MY_ADDRESS = 42;

Ultrasonic ultrasonic(12, 13);
int distance;
unsigned long lastCapture = 0; 
bool ditch = true;

void setup() {
  //Serial Port begin
  Serial.begin (115200);


  // makes first reading
  distance = ultrasonic.read();
  Serial.print("First Distance in CM: ");
  Serial.println(distance);
  lastCapture = millis();
  delay(1000);


  // Start the I2C Bus as Slave on address 9
  Wire.begin(I2C_SLAVE_ADDRESS); 
  
  // Attach a function to trigger when something is received.
  Wire.onRequest(requestEvent);
  Serial.println("startup");
  
}

void requestEvent() {
  // Serial.print("requested: ");

  // // int potvalue = ReadDistance();   

  // int cm = ultrasonic.read();
  // Serial.print("Distance in CM: ");
  // Serial.println(cm);
  // delay(1000);
  //Serial.println(cm);

  ditch = true;
  byte SlaveSend = distance;//map(potvalue,0,1023,0,127);   
  Wire.write(SlaveSend);  
  Serial.println("int");

  //   int X = 150;
  // byte XLow = X & 0xff;
  // byte XHigh = (X >> 8);
  // Wire.write(XHigh);
  // Wire.write(XLow);
  // Wire.write (15);  // send response

  // I2C_writeAnything (3);
  // if (Wire.endTransmission () == 0)
  //     {
        // Serial.println("Sent");

  //     }
  //   else
  //     {
  //             Serial.println("failed");

  //     }

}

void loop() {

  // reads every seconds
  if((millis() - lastCapture) < 500){
    return;
  }

  int tmpDistance = ultrasonic.read(); 

  // if interrupt breaks the capture, the result is discarted. Next reading will be immediate
  if(ditch == true){
    Serial.println("ditched");
    lastCapture = 0;
    ditch = false;
    return;
  }

  distance = tmpDistance;
  Serial.print("Distance in CM: ");
  Serial.println(distance);
  lastCapture = millis();
  // int distance = ultrasonic.read();
  
  // Serial.print("Distance in CM: ");
  // Serial.println(distance);
}


    
