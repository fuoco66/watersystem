#include <Wire.h>
// #include "libs/AdvWire/AdvWire.h"
#include "AdvWire.h"
// #include <I2C_Anything.h>

const byte MY_ADDRESS = 25;
const byte SLAVE_ADDRESS = 42;
#define I2C_SLAVE_ADDRESS 0x08 // Address of the slave

AdvWire advWireInterface;

void setup() 
{
  Serial.begin(115200);
  // Wire.begin (MY_ADDRESS);
  // Wire.onReceive(receiveEvent);

  advWireInterface.begin(MY_ADDRESS, receiveEvent);

  
}  // end of setup

void loop() {
  uint8_t c = 0;

  //read in characters if we got them.
  if (Serial.available() > 0)   {
    Serial.println("commend");

    c = (uint8_t)Serial.read();
    // saves new char
    advWireInterface.addCharToCommand(c);
  }

  if (c == '\n') {
    // advWireInterface.addCharToCommand('S', 2, 0);
    Serial.println("eol");

    advWireInterface.SendCommand(I2C_SLAVE_ADDRESS);
  }

}  // end of loop

void receiveEvent (int howMany){

  // hack to remove warning
  howMany = howMany;
    Serial.println("recieved");

  char res[2];
  I2C_readAnything(res);

  Serial.println((String)res);
  /*for (int i = 0; i < howMany; i++){
    byte b = Wire.read ();
    
    Serial.println((char)b);
  }  // end of for loop*/
  
} // end of receiveEvent


