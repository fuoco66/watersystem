#ifndef AdvWire_h
#define AdvWire_h
#include <Arduino.h>
#include <I2C_Anything.h>

#define COMMAND_SIZE 128

// Advanced Wire class
class AdvWire
{

public:
  volatile boolean haveData;
  uint8_t command_line[COMMAND_SIZE];
  uint8_t recieved_command_line[COMMAND_SIZE];
  uint8_t recieved_sin_count = 0;

  void begin();
  void begin(uint8_t address, void (*onRecieve)(int));

  // add char at the end
  uint8_t addCharToCommand(uint8_t _char);

  // ad char at specific location
  uint8_t addCharToCommand(uint8_t _char, uint8_t pos, uint8_t replace);

  void SendSignal(uint8_t recieverAddr);

  void SendCommand(uint8_t recieverAddr);

  void ManageRecieve(uint8_t howMany);
  void clear_recieved_command_string();

  AdvWire();

private:
  uint8_t sin_count = 0;

  void clear_command_string();
};

#endif
