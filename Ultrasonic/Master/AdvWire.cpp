#include <Arduino.h>
#include "AdvWire.h"

AdvWire::AdvWire(){}

void AdvWire::begin(uint8_t address, void (*onRecieve)(int)){

  Wire.begin(address);
  Wire.onReceive(onRecieve);
}

// master wire
void AdvWire::begin(){
  Wire.begin();
}

uint8_t AdvWire::addCharToCommand(uint8_t _char){
  return this->addCharToCommand(_char, this->sin_count, 1);
}

uint8_t AdvWire::addCharToCommand(uint8_t _char, uint8_t pos, uint8_t replace = 0){

  // if replace false, fix the array to avoid overwrite
  if(!replace){
    // moves all data
    for (int i = this->sin_count -1; i >= pos; i--){
      this->command_line[i + 1] = this->command_line[i];
    }
  }
  
  this->command_line[pos] = _char;
  this->sin_count++;
  return this->sin_count;

}

void AdvWire::ManageRecieve(uint8_t howMany){

  // creates local data packet
  uint8_t payLoad[32];

  // read payload
  I2C_readAnything(payLoad);

  int startIndex = 0;
  for (int i = 0; i < COMMAND_SIZE; i++){
    // look for the first free spot
    if((char)this->recieved_command_line[i] == '%'){
      startIndex = i;
      break;
    }
  }

  for (int j = 0; j < howMany; j++){

    // copy content
    this->recieved_command_line[startIndex] = payLoad[j];

    // increment command index
    startIndex++;

    // increment command size
    if ((char)payLoad[j] != '%'){
      this->recieved_sin_count++;
    }    

  }

  // if the payload is full, check if this is a concatenated message
  if(howMany == 32 && (char)payLoad[31] == '%'){
    return;
  }

  this->haveData = true;
}

void AdvWire::SendSignal(uint8_t recieverAddr){
  this->addCharToCommand('S', 0);
  this->SendCommand(recieverAddr);
}

void AdvWire::SendCommand(uint8_t recieverAddr){

  // command line is empty
  if(!this->sin_count){
    return;
  }

  this->command_line[this->sin_count+1] = 0;

  // calculates chunks
  int chuncksCount = this->sin_count / 32 + 1;

  int sin_index = 0;
  for (int j = 0; j < chuncksCount; j++){
    
    Wire.beginTransmission(recieverAddr);

    while (sin_index < this->sin_count){

      Wire.write(this->command_line[sin_index]);

      sin_index++;
      if (sin_index == 31 * (j + 1)){
        break;
      }
    }

    if (j != chuncksCount - 1){
      Wire.write('%');
    }

    Wire.endTransmission();
  }

  this->clear_command_string();
}

// PRIVATE

void AdvWire::clear_command_string(){
  for (int i = 0; i < COMMAND_SIZE; i++)
    this->command_line[i] = 0;
  this->sin_count = 0;
}

void AdvWire::clear_recieved_command_string(){
  for (int i = 0; i < COMMAND_SIZE; i++)
    this->recieved_command_line[i] = 0;
  this->recieved_sin_count = 0;
  this->haveData = false;
}