#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h> 
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <Ticker.h>

#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <TimedAction.h>
#include <ezTime.h>
#include "ExpanderInterface.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ArduinoMqttClient.h>      //https://github.com/arduino-libraries/ArduinoMqttClient/
#include <LittleFS.h>
#include <MPU9250_asukiaaa.h>

#define STATUS_LED_1  2

// input port of port expander
#define WaterLevelInterface PortExpander

// GPIO ON ESP8266 OLD
// #define PUMP_PIN 14
// #define ONE_WIRE_BUS 13//D7
// #define EXPANDER_PIN_INTERRUPT 12 // Interrupt on this Arduino Uno pin.

// GPIO ON ESP8266 
#define PUMP_PIN 13
#define ONE_WIRE_BUS 12//D7
#define EXPANDER_PIN_INTERRUPT 14 // Interrupt on this Arduino Uno pin.

#define EXPANDER_ADDR 0 // Expander address
#define EXPANDER_USED_PINS 13 // Expander address

// proto
#define PUMP_FLOOD_PIN 7
#define WATER_LEVEL_PIN_0 1
#define WATER_LEVEL_PIN_1 2
#define WATER_LEVEL_PIN_2 0
#define WATER_LEVEL_PIN_3 3
#define WATER_LEVEL_PIN_4 6
#define WATER_LEVEL_PIN_5 4
#define WATER_LEVEL_PIN_6 5

#define STATUS_LED_R 12
#define STATUS_LED_G 13
#define STATUS_LED_B 14
#define BTN_LED 11
#define FAN_PIN 10

#define SDA_PIN 4
#define SCL_PIN 5
#define GAUGE_ADDRESS 0x08
#define SENSOR_RELOAD 30000

#define STATE_IDLE 0
#define STATE_PUMP_ON 1
#define STATE_ERROR_LOW_WATER 2
#define STATE_ERROR_WATER_SENSOR_ERROR 3
#define STATE_ERROR_PUMP_FLOOD 4
#define STATE_ERROR 99 

#define MULTI_BTN_PIN A0

#define ZERO_LIMIT 100
#define BTN_1_LIMIT 550
#define BTN_2_LIMIT 1024
#define BTN_LED_BLINK 200

#define SEALEVELPRESSURE_HPA (1013.25)
#define AMBIENT_SENSOR_ADDR 0x76 // Expander address
#define AMBIENT_SENSOR_DELAY 500//1000

// 1 minute
#define MQTT_RECONNECT_TIME 60000

enum {
  CMD_GET_GAUGE_ID = 1,
  READ_GAUGE = 2
};

struct Settings {

	char host[34];
	char update_username[40];
	char update_password[40];
	char update_path[34];

} SettingsData = {
  // The default values
	"",
  "",
  "",
  "/firmware"
};

typedef struct MqttServer{
  char userName[100];
  char key[100];
  char location[16];
  uint16_t port;
  uint8_t connAttempts;
} MqttServer;

struct ConfigurationSettings {
  bool autoWater;
  uint8_t pumpPin;
  bool pumpPinInverted;
  long int waterTime;
  uint8_t startHour;
  uint8_t startMinutes;
  uint8_t stopHour;
  uint8_t stopMinutes;
  uint16_t waterLevelLow;
  uint16_t waterLevelHigh;
  MqttServer mqttServer;
} ConfigurationData = {
  true,   // autoWater
  13,     // pumpPin
  false,  // pumpPinInverted
  60000,  // waterTime
  7,      // startHour
  0,      // startMinutes
  7,      // stopHour
  3,      // stopMinutes,
  0,    // waterLevelLow
  100,    // waterLevelHigh
  NULL
};

typedef struct AmbientData{
  bool isSensorActive;
  unsigned long int lastReadingTime;
  float temperature;
  float humidity;
  float pressure;
  float altitude;
  float psuTemperature;
} AmbientData;

typedef struct MqttStatus {
  int connected;
  int state;
  uint8_t connCount;
  long int lastConnection;
  long int statusTimer;
} MqttStatus;

struct MqttCache {
  uint8_t machineState;
  bool isPumpActive;
  unsigned long int elapsedTime;
  bool sensorOverride;
  bool testMode;
  uint8_t waterLevel;
  uint8_t temperature;
  uint8_t humidity;
  uint16_t pressure;
  uint16_t altitude;
  uint8_t psuTemperature;
} MqttCache = {
  STATE_ERROR,  // machineState
  false,        // isPumpActive
  0,            // elapsedTime
  false,        // sensorOverride
  false,        // testMode
  0,            // waterLevel
  0,            // temperature
  0,            // humidity
  0,            // pressure
  0,            // altitude
  0             // psuTemperature
};

typedef struct WaterLevelData {
  uint8_t raw;
  uint8_t raw_c;
  uint8_t level;
  uint8_t isLow;
  bool sensorError;
  unsigned long int lastSensorReload;
} WaterLevelData;

struct Status {
  uint8_t machineState;
  bool isPumpActive;
  bool sensorOverride;
  bool testMode;
  bool timedStart;
  bool isPumpFlood;
  unsigned long int elapsedTime;
  unsigned long int waterTime;
  unsigned long int startTime;
  uint8_t lastPressedButton;
  unsigned long int lastDebounceTime;
  unsigned long int lastBlink;
  unsigned long int blinkCount;
  WaterLevelData waterLevelData;
  AmbientData ambientData;
  MqttStatus mqttStatus;
} StatusData = {
  STATE_ERROR,
  false,          // isPumpActive
  false,          // sensorOverride
  false,          // testMode
  false,          // timedStart
  false,          // isPumpFlood
  0,              //elapsedTime
  0,              //waterTime
  0,              //startTime
  0,              //lastPressedButton
  0,              //lastDebounceTime
  0,              //lastBlink
  0,              //blinkCount
  NULL,           //waterLevelData
  NULL,           //ambientData
  NULL            //mqttStatus
};

typedef struct TimeData {
  uint8_t minutes;
  uint8_t seconds;
} TimeData;

void ReadSettings();
void CheckStatus();
void CheckBtns();
void PublishStatusMqtt();
void PublishAmbientDataMqtt();
void BlinkButtonLed();
bool InitAmbientSensor();

void TimedStart();
#define DEBUG 1

#if DEBUG
  #define Sbegin(x) (Serial.begin(x))
  #define Sprintln(x) (Serial.println(x))
  #define Sprint(x) (Serial.print(x))
  #define Sprintf(x,y) (Serial.print(x))
#else
  #define Sbegin(x)
  #define Sprintln(x)
  #define Sprint(x)
#endif

//  {
//     "board": "esp8266:esp8266:nodemcuv2",
//     "configuration": "xtal=80,vt=flash,exception=legacy,ssl=all,eesz=4M2M,led=2,ip=lm2f,dbg=Disabled,lvl=None____,wipe=none,baud=115200",
//     "port": "COM6"
// }
