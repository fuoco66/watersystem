
//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  StatusLedTicker.attach(1, tick);
}

//callback notifying us of the need to save config
void saveConfigCallback () {
  shouldSaveConfig = true;
}

void SetupConfigMode(WiFiManager& wifiManager){
  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name autoWater/prompt default length
  WiFiManagerParameter custom_text0("<p>Select your wifi network and type in your password, if you do not see your wifi then scroll down to the bottom and press scan to check again.</p>");
  WiFiManagerParameter custom_host_title("<h1>Hostname</h1>");
  WiFiManagerParameter custom_host_p1("<p>Enter a name for this device which will be used for the hostname on your network and identify the device from MQTT.</p>");
  WiFiManagerParameter custom_host_value("name", "Device Name", SettingsData.host, 32);

  WiFiManagerParameter custom_text5("<h1>Web Updater</h1>");
  WiFiManagerParameter custom_text6("<p>The web updater allows you to update the firmware of the device via a web browser by going to its ip address or hostname /firmware ex. 192.168.0.5/firmware you can change the update path below. The update page is protected so enter a username and password you would like to use to access it. </p>");
  WiFiManagerParameter custom_update_username("user", "Username For Web Updater", SettingsData.update_username, 40);
  WiFiManagerParameter custom_update_password("password", "Password For Web Updater", SettingsData.update_password, 40);
  WiFiManagerParameter custom_device_path("path", "Updater Path", SettingsData.update_path, 32);
  WiFiManagerParameter custom_text7("<p>*To reset device settings restart the device and quickly move the jumper from RUN to PGM, wait 10 seconds and put the jumper back to RUN.*</p>");
  WiFiManagerParameter custom_text8("");

  //set static ip
  //_manager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0));
  
  //add all your parameters here
  wifiManager.setCustomHeadElement("<style>.c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{text-align: center;font-family:oswald;} button{border:0;background-color:#313131;color:white;line-height:2.4rem;font-size:1.2rem;text-transform: uppercase;width:100%;font-family:oswald;} .q{float: right;width: 65px;text-align: right;} body{background-color: #575757;}h1 {color: white; font-family: oswald;}p {color: white; font-family: open+sans;}a {color: #78C5EF; text-align: center;line-height:2.4rem;font-size:1.2rem;font-family:oswald;}</style>");
  wifiManager.addParameter(&custom_text0);
  wifiManager.addParameter(&custom_host_title);
  wifiManager.addParameter(&custom_host_p1);
  wifiManager.addParameter(&custom_host_value);

  wifiManager.addParameter(&custom_text5);
  wifiManager.addParameter(&custom_text6);
  wifiManager.addParameter(&custom_update_username);
  wifiManager.addParameter(&custom_update_password);
  wifiManager.addParameter(&custom_device_path);
  wifiManager.addParameter(&custom_text7);
  wifiManager.addParameter(&custom_text8);
  
  wifiManager.setTimeout(120);
  
  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect(ssidAP.c_str())) {
    Sprintln("2 minutes passed, reset and try connecting again");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Sprintln("connected...yeey :)");
  
  if (shouldSaveConfig) {
    strcpy(SettingsData.host, custom_host_value.getValue());
    strcpy(SettingsData.update_username, custom_update_username.getValue());
    strcpy(SettingsData.update_password, custom_update_password.getValue());
    strcpy(SettingsData.update_path, custom_device_path.getValue());
  
    SaveSettings();
  }

  delay(5000);

  // verify if params are complete
  if(digitalRead(0) == LOW ||
    String(SettingsData.host).length() == 0 ||
    String(SettingsData.update_username).length() == 0 ||
    String(SettingsData.update_password).length() == 0){

    Sprint("host: ");
    Sprintln(SettingsData.host);
    
    Sprint("usr: ");
    Sprintln(SettingsData.update_username);
    
    Sprint("psw: ");
    Sprintln(SettingsData.update_password);
    
    Sprint("dig: ");
    Sprintln(digitalRead(0));
        
    Sprintln("Reset, Params corrupted");
    
    FullReset();
    // wifiManager.resetSettings();
    // ESP.reset();
  }

  StatusLedTicker.detach();
  //keep LED on

}

void HandleWebRoot(){

  ObjGlobal_HttpServer.sendHeader("Location", "/index.html",true);   //Redirect to our html web page
  ObjGlobal_HttpServer.send(302, "text/plane","");
}

void handleWebRequests(){  
  if(loadFromLittleFS(ObjGlobal_HttpServer.uri())) return;
  String message = "File Not Detected\n\n";
  message += "URI: ";
  message += ObjGlobal_HttpServer.uri();
  message += "\nMethod: ";
  message += (ObjGlobal_HttpServer.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += ObjGlobal_HttpServer.args();
  message += "\n";
  for (uint8_t i=0; i<ObjGlobal_HttpServer.args(); i++){
    message += " NAME:"+ObjGlobal_HttpServer.argName(i) + "\n VALUE:" + ObjGlobal_HttpServer.arg(i) + "\n";
  }
  ObjGlobal_HttpServer.send(404, "text/plain", message);
  Serial.println(message);
}

bool loadFromLittleFS(String path){
  String dataType = "text/plain";
  if(path.endsWith("/")) path += "index.htm";

  if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
  else if(path.endsWith(".html")) dataType = "text/html";
  else if(path.endsWith(".htm")) dataType = "text/html";
  else if(path.endsWith(".css")) dataType = "text/css";
  else if(path.endsWith(".js")) dataType = "application/javascript";
  else if(path.endsWith(".png")) dataType = "image/png";
  else if(path.endsWith(".gif")) dataType = "image/gif";
  else if(path.endsWith(".jpg")) dataType = "image/jpeg";
  else if(path.endsWith(".ico")) dataType = "image/x-icon";
  else if(path.endsWith(".xml")) dataType = "text/xml";
  else if(path.endsWith(".pdf")) dataType = "application/pdf";
  else if(path.endsWith(".zip")) dataType = "application/zip";
  File dataFile = LittleFS.open(path.c_str(), "r");
  if (ObjGlobal_HttpServer.hasArg("download")) dataType = "application/octet-stream";
  if (ObjGlobal_HttpServer.streamFile(dataFile, dataType) != dataFile.size()) {
  }

  dataFile.close();
  return true;
}

void HandleOsConfig(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-1\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config_HandleOsConfig");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-4\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-2\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

if(objPayload["action"] == "getConfig"){
    // serializeJson(WebGetButtonsConfig(), Serial);

    String webResponse;
	  serializeJson(WebGetConfig(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "saveConfig"){

    SaveConfigFile(strPayload, configfile);
    // serializeJson(WebGetButtonsConfig(), Serial);
	  //reload configuration in memory
	  LoadConfig();

    // reload timed events
    // SetupTime();

    //reload Mqtt
    SetupMqtt();
    
    String webResponse;
    serializeJson(WebGetConfig(), webResponse);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  } 
  else {
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-3\", \"response\":\"ACTION_NOT_FOUND\"}");
  }

}

void HandleOsStatus(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-9\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config_HandleOsStatus");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-10\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-11\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "GetStatus"){
    // serializeJson(WebGetButtonsConfig(), Serial);

    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");

  }
  else if(objPayload["action"] == "GetReloadStatus"){
    
    ReloadStatus();
    
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  } 
  else {
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }
}

void HandleOsAction(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-5\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config_HandleOAction");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-6\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-7\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "pumpOn"){
    
    StatusData.sensorOverride = objPayload["value"]["overrideSensor"].as<bool>();

    PumpON();
    
    // ReloadStatus();
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  }
  else if(objPayload["action"] == "pumpOff"){

    StatusData.sensorOverride = objPayload["value"]["overrideSensor"].as<bool>();

    PumpOFF();
    
    // ReloadStatus();
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  }
  else if(objPayload["action"] == "timedStart"){
    
    StatusData.sensorOverride = objPayload["value"]["overrideSensor"].as<bool>();
    StatusData.waterTime = objPayload["value"]["timeToRun"].as<long int>();

    TimedStart();
    
    // ReloadStatus();
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  }
  else if(objPayload["action"] == "overrideSensors"){

    StatusData.sensorOverride = objPayload["value"]["overrideSensor"].as<bool>();
    
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  }
  else if(objPayload["action"] == "testMode"){

    bool value = objPayload["value"]["testMode"].as<bool>();
    ToggleTestModeOn(value);
   
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  }
  // else if(objPayload["action"] == "fanOn"){
    
  //   FanON();
    
  //   // ReloadStatus();
  //   String webResponse;
	//   serializeJson(WebGetCachedStatus(), webResponse);
	//   ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  // }
  // else if(objPayload["action"] == "fanOff"){

  //   FanOFF();
    
  //   // ReloadStatus();
  //   String webResponse;
	//   serializeJson(WebGetCachedStatus(), webResponse);
	//   ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  // }
  else if(objPayload["action"] == "mqttConnect"){

    // resets connection count, to force mqtt reconnection
    StatusData.mqttStatus.connCount = 0;
    
    String webResponse;
	  serializeJson(WebGetCachedStatus(), webResponse);
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":" + webResponse + "}");
 
  }
  else {
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }

}

void HandleOsSystem(){

  // get payload
  String strPayload = ObjGlobal_HttpServer.arg("plain");

  // check payload
  if(strPayload == "" || strPayload.length() == 0 || strPayload == "\"\"" || strPayload == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-9\", \"response\":\"EMPTY_PAYLOAD\"}");
    return;
  }

  // parse argument
  DynamicJsonDocument objPayload(2024);
  DeserializationError jsonError = deserializeJson(objPayload, strPayload);
			
  if (jsonError){
    Sprintln("failed to load json config_HandleOsConfig");
    Sprintln(jsonError.c_str());
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-10\", \"response\":\"DECODE_FAILED\"}");
    return;
  }
  // release some memory
  objPayload.shrinkToFit();

  // check action
  if(objPayload["action"] == "" || objPayload["action"] == "\"\"" || objPayload["action"] == "null"){
    Sprintln(strPayload);
    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-11\", \"response\":\"EMPTY_ACTION\"}");
    return;
  }

  if(objPayload["action"] == "reboot"){

	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");
    Sprintln("Reboot");
    delay(1000);
    Reboot();

  }
  else if(objPayload["action"] == "wifiReset"){
    
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");
    Sprintln("Wifi reset");
    delay(1000);
    WifiReset();
    
  } 
  else if(objPayload["action"] == "fullReset"){
    
	  ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"0\", \"response\":\"OK\"}");
    Sprintln("Full Reset");
    delay(1000);
    FullReset();
  }
  else {
    Sprintln(strPayload);

    ObjGlobal_HttpServer.send(200, "text/json","{\"code\": \"-8\", \"response\":\"ACTION_NOT_FOUND\"}");
  }
}

void SetupWebHandlers(){
  // setup root page
  
  ObjGlobal_HttpServer.on("/", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    HandleWebRoot();
  });

  // reset, reboot, wipe
  ObjGlobal_HttpServer.on("/osSystem", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsSystem();

  });

  ObjGlobal_HttpServer.on("/osAction", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsAction();

  });

  ObjGlobal_HttpServer.on("/osStatus", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsStatus();

  });


  // setup reset page
  ObjGlobal_HttpServer.on("/osConfig", [](){
    if(!ObjGlobal_HttpServer.authenticate(SettingsData.update_username, SettingsData.update_password))
      return ObjGlobal_HttpServer.requestAuthentication();
    
    HandleOsConfig();

  });

  ObjGlobal_HttpServer.onNotFound(handleWebRequests);

  ObjGlobal_HttpServer.begin();
  
}