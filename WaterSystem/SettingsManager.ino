void ReadSettings(){

	if (LittleFS.begin()) {
		Sprintln("Settings Spiff begin");

		if (LittleFS.exists(settingsfile)) {
			
			Sprintln("Settings File found");
			
			//file exists, reading and loading
			File SettingsFile = LittleFS.open(settingsfile, "r");

			if (SettingsFile) {
				Sprintln("Settings File opened");

				size_t size = SettingsFile.size();
				// Allocate a buffer to store contents of the file.
				std::unique_ptr<char[]> buf(new char[size]);

				SettingsFile.readBytes(buf.get(), size);
				// DynamicJsonBuffer jsonBuffer;
				DynamicJsonDocument jsonBuffer(size);
				// JsonObject json = jsonBuffer.parseObject(buf.get());
				
				DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
			
				if (jsonError){
					Sprintln("failed to load json Settings");
					SettingsFile.close();
  				return;
				}

				Sprintln("");
				// print JSON to serial
				// serializeJson(jsonBuffer, Serial);
				Sprintln("");

				Sprintln("Load settings");

				strcpy(SettingsData.host, jsonBuffer["host"]);
				Sprintln("Loadaded host");

				strcpy(SettingsData.update_username, jsonBuffer["update_username"]);
				Sprintln("Loadaded update_username");

				strcpy(SettingsData.update_password, jsonBuffer["update_password"]);
				Sprintln("Loadaded update_password");

				strcpy(SettingsData.update_path, jsonBuffer["update_path"]);
				Sprintln("Loadaded update_path");

				// close file
				SettingsFile.close();

			}
		}
		else{
			Sprintln("File NOT found");
		}
	} 
	else {
		Sprintln("SPIFF FAILED");
	}
}

void SaveSettings(){

	DynamicJsonDocument jsonDoc(1024);
	jsonDoc["host"] = SettingsData.host;

	jsonDoc["update_username"] = SettingsData.update_username;
	jsonDoc["update_password"] = SettingsData.update_password;
	jsonDoc["update_path"] = SettingsData.update_path;

	File SettingsFile = LittleFS.open(settingsfile, "w");
	if (!SettingsFile) {
	}

	// serializeJson(jsonDoc, Serial);
	serializeJson(jsonDoc, SettingsFile);

	SettingsFile.close();
}


bool CreateConfigFile(){
	
	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();

	// load default data from struct init	
	root["autoWater"] = ConfigurationData.autoWater;
	root["pumpPin"] = ConfigurationData.pumpPin;
	root["pumpPinInverted"] = ConfigurationData.pumpPinInverted;
	
	root["waterTime"] = ConfigurationData.waterTime;

	root["waterLevelLow"] = ConfigurationData.waterLevelLow;
	root["waterLevelHigh"] = ConfigurationData.waterLevelHigh;

	root["startHour"] = ConfigurationData.startHour;
	root["startMinutes"] = ConfigurationData.startMinutes;
	root["stopHour"] = ConfigurationData.stopHour;
	root["stopMinutes"] = ConfigurationData.stopMinutes;

	JsonObject mqttServer = root.createNestedObject("mqttServer");

	mqttServer["userName"] = ConfigurationData.mqttServer.userName;
	mqttServer["key"] = ConfigurationData.mqttServer.key;
	mqttServer["location"] = ConfigurationData.mqttServer.location;
	mqttServer["port"] = ConfigurationData.mqttServer.port;
	mqttServer["connAttempts"] = ConfigurationData.mqttServer.connAttempts;

	File ConfigFile = LittleFS.open(configfile, "w");
	if (!ConfigFile) {
		Sprintln("Failed to create file");
		return false;
	}

	serializeJson(root, Serial);
	serializeJson(root, ConfigFile);

	ConfigFile.close();
	Sprintln("File created");

	return true;
}

// Load the buttons config from file
void LoadConfig(){

	if (!LittleFS.begin()) {
		Sprintln("Config SPIFF FAILED");
		return;
	}
	
	Sprintln("Config Spiff begin");

	if(!LittleFS.exists(configfile)) {
		Sprintln("File NOT found, creating");
		if(CreateConfigFile() != true){
			return;
		}
	}
		
	Sprintln("Config File found");
	
	//file exists, reading and loading
	File objConfigFile = LittleFS.open(configfile, "r");

	if(!objConfigFile){
		Sprintln("Failed to open Button file");
		return;
	}

	// Sprintln("Button File opened");
	// for debug, print the whole file
	// while (objConfigFile.available()) {

	//     Serial.write(objConfigFile.read());
	// }

	size_t size = objConfigFile.size();
	// Allocate a buffer to store contents of the file.
	std::unique_ptr<char[]> buf(new char[size]);

	objConfigFile.readBytes(buf.get(), size);

	DynamicJsonDocument jsonBuffer(1024);
	DeserializationError jsonError = deserializeJson(jsonBuffer, buf.get());
	
	if (jsonError){
		Sprintln("Failed to parse config json: ");
		switch (jsonError.code()) {
			case DeserializationError::InvalidInput:
					Sprintln(F("Invalid input!"));
					break;
			case DeserializationError::NoMemory:
					Sprintln(F("Not enough memory"));
					break;
			default:
					Sprintln(F("Deserialization failed"));
					break;
		}

		objConfigFile.close();
		return;
	}

	Sprintln("config File Loaded");

	ConfigurationData.autoWater = jsonBuffer["autoWater"].as<bool>();
	// OVERRIDE
	ConfigurationData.pumpPin = PUMP_PIN;
	// ConfigurationData.pumpPin = jsonBuffer["pumpPin"];
	ConfigurationData.pumpPinInverted = jsonBuffer["pumpPinInverted"].as<bool>();
	
	ConfigurationData.waterTime = jsonBuffer["waterTime"];

	ConfigurationData.waterLevelLow = jsonBuffer["waterLevelLow"];
	ConfigurationData.waterLevelHigh = jsonBuffer["waterLevelHigh"];

	ConfigurationData.startHour = jsonBuffer["startHour"];
	ConfigurationData.startMinutes = jsonBuffer["startMinutes"];
	ConfigurationData.stopHour = jsonBuffer["stopHour"];
	ConfigurationData.stopMinutes = jsonBuffer["stopMinutes"];
	
	strcpy(ConfigurationData.mqttServer.userName, jsonBuffer["mqttServer"]["userName"].as<char*>());
	strcpy(ConfigurationData.mqttServer.key, jsonBuffer["mqttServer"]["key"].as<char*>());
	strcpy(ConfigurationData.mqttServer.location, jsonBuffer["mqttServer"]["location"].as<char*>());
	ConfigurationData.mqttServer.port = jsonBuffer["mqttServer"]["port"];
	ConfigurationData.mqttServer.connAttempts = jsonBuffer["mqttServer"]["connAttempts"];


}

// load the config from memory
DynamicJsonDocument WebGetConfig(){

	DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();

	root["autoWater"] = ConfigurationData.autoWater;
	root["pumpPin"] = ConfigurationData.pumpPin;
	root["pumpPinInverted"] = ConfigurationData.pumpPinInverted;

	root["waterTime"] = ConfigurationData.waterTime;
	
	root["waterLevelLow"] = ConfigurationData.waterLevelLow; 
	root["waterLevelHigh"] = ConfigurationData.waterLevelHigh;

	root["startHour"] = ConfigurationData.startHour;
	root["startMinutes"] = ConfigurationData.startMinutes;
	root["stopHour"] = ConfigurationData.stopHour;
	root["stopMinutes"] = ConfigurationData.stopMinutes;
	
	JsonObject mqttServer = root.createNestedObject("mqttServer");

	mqttServer["userName"] = ConfigurationData.mqttServer.userName;
	mqttServer["key"] = ConfigurationData.mqttServer.key;
	mqttServer["location"] = ConfigurationData.mqttServer.location;
	mqttServer["port"] = ConfigurationData.mqttServer.port;
	mqttServer["connAttempts"] = ConfigurationData.mqttServer.connAttempts;


	return jsonDoc;

}

void SaveConfigFile(String payLoad, String strFileName){

	DynamicJsonDocument jsonBuffer(2048);
	DeserializationError jsonError = deserializeJson(jsonBuffer, payLoad);

	if (jsonError){
		Sprintln("Failed to parse json");
		return;
	}

	File objFile = LittleFS.open(strFileName, "w");
	if (!objFile) {
		Sprintln("Failed to open file");
		return;
	}

	Sprintln("Writing file");
	// serializeJson(jsonBuffer["value"], Serial);

	serializeJson(jsonBuffer["value"], objFile);
	Sprintln("File Written");

	objFile.close();
		
	Sprintln("Writing END");
	
}

void WifiReset(){
	ObjGlobal_WifiManager.resetSettings();
	ESP.reset();
}

void FullReset(){

	if (LittleFS.begin()) {

		if (LittleFS.exists(settingsfile)) {
			LittleFS.remove(settingsfile);
		}
		
		if (LittleFS.exists(configfile)) {
			LittleFS.remove(configfile);
		}

		LittleFS.end();

	}
  WifiReset();
}

void Reboot(){
	ESP.reset();
}
