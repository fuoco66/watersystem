void GaugeSendCommand (const byte cmd, const int responseSize){
  Wire.beginTransmission(GAUGE_ADDRESS);
  Wire.write(cmd);
  Wire.endTransmission();

  if (Wire.requestFrom (GAUGE_ADDRESS, 1) == 0) {
    // Sprintln("Command Failed");
  }
  else{
    // Sprintln("Command Success");
  }
  
}

void SetupWaterLevelSensor(){

  Wire.begin(SDA_PIN, SCL_PIN);

  StatusData.waterLevelData.lastSensorReload = millis();
  StatusData.waterLevelData.sensorError = false;

  // send initialize command
  GaugeSendCommand(CMD_GET_GAUGE_ID, 1);
  
  // wait for response
  if(Wire.available ()){
    Serial.print("Gauge is ID: ");
    Sprintln(Wire.read ());
  }
  else{
    Sprintln("No response to ID request");
  }  // end of setup

}

void ReloadWaterLevelData(){

  uint8_t sensorId;

  unsigned long int currentTime = millis();

  if(StatusData.waterLevelData.sensorError == true){

    unsigned long int passedTime = currentTime - StatusData.waterLevelData.lastSensorReload;
    if(passedTime < SENSOR_RELOAD){
      return;
    }
    Sprintln("Sensor Reload");
    SetupWaterLevelSensor();
  }

  // Send read request
  GaugeSendCommand(READ_GAUGE, 1);

  // 
  if (Wire.available ()){
    // Sprint("Recieved: ");
    StatusData.waterLevelData.raw = Wire.read ();
  }
  else{
    Sprintln("No response from gauge");
    StatusData.waterLevelData.raw = 255;
    StatusData.waterLevelData.raw_c = 255;
    StatusData.waterLevelData.level = 255;
    StatusData.waterLevelData.isLow = true;
    StatusData.waterLevelData.sensorError = true;
    return;
  }  // end of setup

  if (StatusData.waterLevelData.raw <= ConfigurationData.waterLevelLow){
    StatusData.waterLevelData.raw = 0;
    StatusData.waterLevelData.level = 0;
    StatusData.waterLevelData.isLow = true;
    return;
  }
  
  // constrain the value between the configures values
  StatusData.waterLevelData.raw_c = constrain(StatusData.waterLevelData.raw, ConfigurationData.waterLevelLow, ConfigurationData.waterLevelHigh);

  // map(value, fromLow, fromHigh, toLow, toHigh)
  StatusData.waterLevelData.level = map(StatusData.waterLevelData.raw_c, ConfigurationData.waterLevelLow, ConfigurationData.waterLevelHigh, 0, 100);

  StatusData.waterLevelData.isLow = false;
  
  // Sprint("Raw: ");
  // Sprintln(StatusData.waterLevelData.raw);
  // // Sprint("Raw_c: ");
  // // Sprintln(StatusData.waterLevelData.raw_c);
  // Sprint("level: ");
  // Sprintln(StatusData.waterLevelData.level);
  // Sprint("isLow: ");
  // Sprintln(StatusData.waterLevelData.isLow);
  // Sprint("Error: ");
  // Sprintln(StatusData.waterLevelData.sensorError);
  // Sprint("--------------------------");

}