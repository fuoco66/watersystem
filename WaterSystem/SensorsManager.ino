bool InitAmbientSensor(){
  
  bool status = AmbientSensor.begin(0x76);   
  
  StatusData.ambientData = (AmbientData){status, 0, 0, 0, 0, 0, 0};
  
  // initialize power supply temperature sensor
  psuTemperatureSensor.begin(); 
  psuTemperatureSensor.setWaitForConversion(false);

  return status;

}

void ReloadAmbientData(){

  unsigned long currentMillis = millis();
	
	// if debouncing input is ignored
	if(currentMillis - StatusData.ambientData.lastReadingTime <= AMBIENT_SENSOR_DELAY) {
		return;
	}

  // get PSU sensor temperature
  psuTemperatureSensor.requestTemperaturesByAddress(GlobalPsuSensorAddress);
  StatusData.ambientData.psuTemperature = psuTemperatureSensor.getTempC(GlobalPsuSensorAddress);

  // if ambient sensor not available, do nothing
  if(!StatusData.ambientData.isSensorActive){
    return;
  }

	// update time
	StatusData.ambientData.lastReadingTime = currentMillis;

  StatusData.ambientData.temperature = AmbientSensor.readTemperature();
  StatusData.ambientData.humidity = AmbientSensor.readHumidity();
  StatusData.ambientData.pressure = AmbientSensor.readPressure() / 100.0F;
  StatusData.ambientData.altitude = AmbientSensor.readAltitude(SEALEVELPRESSURE_HPA);

  // check if reading is correct
  if(StatusData.ambientData.temperature == NAN ||
     StatusData.ambientData.humidity == NAN ||
     StatusData.ambientData.pressure == NAN ||
     StatusData.ambientData.altitude == NAN ){

    // turn off the sensor, need reset to restart
    StatusData.ambientData = (AmbientData){false, 0, 0, 0, 0, 0, 0};
  }

}