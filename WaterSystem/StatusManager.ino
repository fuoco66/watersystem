void SetupHardware(){

	pinMode(ConfigurationData.pumpPin, OUTPUT);
  digitalWrite(ConfigurationData.pumpPin, LOW);

  pinMode(EXPANDER_PIN_INTERRUPT, INPUT);

	// setup water level system
	pinMode(EXPANDER_PIN_INTERRUPT, INPUT);
  ExpanderInterface::ExpanderPin arrSwitch[EXPANDER_USED_PINS];
  arrSwitch[0] = WaterLevelInterface.CreateInputPin(PUMP_FLOOD_PIN);

	// not used but is better leave it there
  arrSwitch[1] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_0);
  arrSwitch[2] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_1);
  arrSwitch[3] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_2);
	arrSwitch[4] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_3);
  arrSwitch[5] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_4);
  arrSwitch[6] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_5);
  arrSwitch[7] = WaterLevelInterface.CreateInputPin(WATER_LEVEL_PIN_6);

  arrSwitch[8] = WaterLevelInterface.CreateOutputPin(STATUS_LED_R, LOW);
  arrSwitch[9] = WaterLevelInterface.CreateOutputPin(STATUS_LED_G, LOW);
  arrSwitch[10] = WaterLevelInterface.CreateOutputPin(STATUS_LED_B, LOW);
  arrSwitch[11] = WaterLevelInterface.CreateOutputPin(BTN_LED, LOW);
  arrSwitch[12] = WaterLevelInterface.CreateOutputPin(FAN_PIN, LOW);

  WaterLevelInterface.begin(arrSwitch, EXPANDER_USED_PINS, EXPANDER_ADDR, EXPANDER_PIN_INTERRUPT);
	
	InitAmbientSensor();

  SetupWaterLevelSensor();
	
}

void ReloadStatus(){

	int isPumpActive;
	
	// if we are not in test mode, the pin pump is read
	if(!StatusData.testMode){

		if(!ConfigurationData.pumpPinInverted){
			isPumpActive = digitalRead(ConfigurationData.pumpPin);
		}
		else{
			isPumpActive = !digitalRead(ConfigurationData.pumpPin);
		}
	}
	else{
		isPumpActive = StatusData.isPumpActive;		
	}

	StatusData.isPumpActive = isPumpActive;
	
	GetPortExpanderStatus();

	ReloadWaterLevelData();
	
	ReloadAmbientData();
	
	CheckMqttStatus();

}

void GetPortExpanderStatus(){
	
	// vector containing port status
	// if false, is 1. Everything mus be inverted
	uint8_t portStatus[8];
  WaterLevelInterface.ReadPort(0, portStatus);

	// Serial.print("chached value: ");
  // for (int i = 0; i < 8; i++)
  // {
  //   Serial.print(i);
  //   Serial.print("-");
  // }
  // Serial.println();
  // Serial.println();

  // Serial.print("chached value: ");

  // for (int i = 0; i < 8; i++)
  // {
  //   Serial.print(portStatus[i]);
  //   Serial.print("-");
  // }
  // Serial.println("--------------");

	// get pump flood data before reverse
	StatusData.isPumpFlood = !portStatus[PUMP_FLOOD_PIN];

	// Old system
	// uint8_t waterLevelPin = portStatus[WATER_LEVEL_PIN_1];
	
	// if(waterLevelPin == 0){
	//   // Serial.println("is zero");

	// 	StatusData.waterLevelData.raw = 100;
	// 	StatusData.waterLevelData.raw_c = 100;
	// 	StatusData.waterLevelData.level = 100;
	// 	StatusData.waterLevelData.isLow = false;
	// 	StatusData.waterLevelData.sensorError = false;
	// }
	// else {
	//   // Serial.println("is one");
	// 	StatusData.waterLevelData.raw = 0;
	// 	StatusData.waterLevelData.raw_c = 0;
	// 	StatusData.waterLevelData.level = 0;
	// 	StatusData.waterLevelData.isLow = true;
	// 	StatusData.waterLevelData.sensorError = false;
	// }
	
}

DynamicJsonDocument WebGetCachedStatus(){

	DynamicJsonDocument jsonDoc(2048);
	JsonObject root = jsonDoc.to<JsonObject>();

	root["machineState"] = StatusData.machineState;
	root["isPumpActive"] = StatusData.isPumpActive;
	root["isPumpFlood"] = StatusData.isPumpFlood;

	root["sensorOverride"] = StatusData.sensorOverride;
	root["testMode"] = StatusData.testMode;
	root["elapsedTime"] = StatusData.elapsedTime;
	root["waterTime"] = StatusData.waterTime;
	root["timedStart"] = StatusData.timedStart;

	JsonObject ambientData = root.createNestedObject("ambientData");

	ambientData["temperature"] = StatusData.ambientData.temperature;
	ambientData["humidity"] = StatusData.ambientData.humidity;
	ambientData["pressure"] = StatusData.ambientData.pressure;
	ambientData["altitude"] = StatusData.ambientData.altitude;
	ambientData["psuTemperature"] = StatusData.ambientData.psuTemperature;
	
	JsonObject waterLevelData = root.createNestedObject("waterLevelData");

	waterLevelData["raw"] = StatusData.waterLevelData.raw;
	waterLevelData["raw_c"] = StatusData.waterLevelData.raw_c;
	waterLevelData["level"] = StatusData.waterLevelData.level;
	waterLevelData["isLow"] = StatusData.waterLevelData.isLow;

	JsonObject mqttStatus = root.createNestedObject("mqttStatus");

	mqttStatus["connected"] = StatusData.mqttStatus.connected;
	mqttStatus["state"] = StatusData.mqttStatus.state;
	mqttStatus["connCount"] = StatusData.mqttStatus.connCount;

	return jsonDoc;

}

void CheckStatus(){

	// ensure that the sensors status is always updated
	ReloadStatus();
	
	// check for errors
	uint8_t localState = CheckErrors();

	// if != from STATE_IDLE there is an error
	if(localState != STATE_IDLE){
		PumpOFF();
		ChangeMachineState(localState);
		return;
	}

	// pump is OFF, no errors, state idle
	if(!StatusData.isPumpActive){
		ChangeMachineState(STATE_IDLE);
		return;
	}

	// pump is on, updates state
	ChangeMachineState(STATE_PUMP_ON);

	StatusData.elapsedTime = millis() - StatusData.startTime;
	
	if(StatusData.timedStart){

		if(StatusData.elapsedTime >= StatusData.waterTime){
			Sprintln("TIME FINISHED, PUMP OFF");
			ChangeMachineState(STATE_IDLE);
			PumpOFF();
			return;
		}
	}

}

uint8_t CheckErrors(){

	if(StatusData.sensorOverride){
		return STATE_IDLE;
	}
	
	// check for errors
	// if sensor override is true skip controls
	if(StatusData.waterLevelData.isLow && StatusData.waterLevelData.level == 255){
		return STATE_ERROR_WATER_SENSOR_ERROR;
	}
	else if(StatusData.isPumpFlood){
		return STATE_ERROR_PUMP_FLOOD;
	}
	else if(StatusData.waterLevelData.isLow){
		return STATE_ERROR_LOW_WATER;
	}
	
	
}

void ChangeMachineState(uint8_t state){
  if(StatusData.machineState == state){
      return;
    }

    StatusData.machineState = state;
		StatusLed(state);
}

void StatusLed(uint8_t state){

	switch (state)	{
		case STATE_IDLE: // blue
			PortExpander.WritePin(STATUS_LED_B, LOW);
			PortExpander.WritePin(STATUS_LED_G, HIGH);
			PortExpander.WritePin(STATUS_LED_R, HIGH);
			
			PortExpander.WritePin(BTN_LED, LOW);

			break;

		case STATE_PUMP_ON: // green
			PortExpander.WritePin(STATUS_LED_B, HIGH);
			PortExpander.WritePin(STATUS_LED_G, LOW);
			PortExpander.WritePin(STATUS_LED_R, HIGH);

			PortExpander.WritePin(BTN_LED, LOW);

		break;

		case STATE_ERROR_LOW_WATER: // yellow
			Sprintln("LOW WATER, PUMP OFF");
			PortExpander.WritePin(STATUS_LED_B, HIGH);
			PortExpander.WritePin(STATUS_LED_G, LOW);
			PortExpander.WritePin(STATUS_LED_R, LOW);

			PortExpander.WritePin(BTN_LED, HIGH);

		break;

		case STATE_ERROR_WATER_SENSOR_ERROR: // purple
			Sprintln("WATER LEVEL SENSOR ERROR, PUMP OFF");
			PortExpander.WritePin(STATUS_LED_B, LOW);
			PortExpander.WritePin(STATUS_LED_G, HIGH);
			PortExpander.WritePin(STATUS_LED_R, LOW);

			PortExpander.WritePin(BTN_LED, HIGH);

		break;

		case STATE_ERROR_PUMP_FLOOD: // red
			Sprintln("PUMP FLOOD, PUMP OFF");
			PortExpander.WritePin(STATUS_LED_B, HIGH);
			PortExpander.WritePin(STATUS_LED_G, HIGH);
			PortExpander.WritePin(STATUS_LED_R, LOW);

			PortExpander.WritePin(BTN_LED, HIGH);

		break;

		case STATE_ERROR: // white, generic error
			PortExpander.WritePin(STATUS_LED_B, LOW);
			PortExpander.WritePin(STATUS_LED_G, LOW);
			PortExpander.WritePin(STATUS_LED_R, LOW);

			PortExpander.WritePin(BTN_LED, HIGH);

		break;

	default: // unkown state, led off
		PortExpander.WritePin(STATUS_LED_B, HIGH);
		PortExpander.WritePin(STATUS_LED_G, HIGH);
		PortExpander.WritePin(STATUS_LED_R, HIGH);
		PortExpander.WritePin(BTN_LED, HIGH);

		break;
	}	

}
