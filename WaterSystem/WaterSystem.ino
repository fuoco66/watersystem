#include "Hardware.h"

//Form Custom SSID
String ssidAP = "WaterSystem" + String(ESP.getChipId());

//flag for saving data
bool shouldSaveConfig = false;

const char* htmlfile = "/index.html";
const char* settingsfile = "/settings.json";
const char* configfile = "/config.json";
uint8_t GlobalBtnLedStateForBlink = LOW;

ESP8266WebServer ObjGlobal_HttpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiManager ObjGlobal_WifiManager;

// WiFiClient net;
Ticker StatusLedTicker;

// HwBtn InputBtn[COUNT_HWBTN];
TimedAction CheckStatusThread = TimedAction(500, CheckStatus);
TimedAction BtnsThread = TimedAction(50, CheckBtns);
TimedAction BlinkThread = TimedAction(100, BlinkButtonLed);

TimedAction PublishMqttStatusThread = TimedAction(1000, PublishStatusMqtt);
TimedAction PublishMqttAmbientThread = TimedAction(60000, PublishAmbientDataMqtt);

const char GlobalAvailableTopic[] = "home/waterSystem/availability";
const char GlobalActionTopic[]  = "home/waterSystem/action";
const char GlobalStatusTopic[]  = "home/waterSystem/statusData";
const char GlobalAmbientDataTopic[]  = "home/waterSystem/ambientData";
long int GlobalKeepAlive = 60000;
long int GlobalStatusTimer = 30000;

ExpanderInterface PortExpander;

// ambientSensor
Adafruit_BME280 AmbientSensor;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature psuTemperatureSensor(&oneWire);
uint8_t GlobalPsuSensorAddress[8] = {0x28, 0x0A, 0x6A, 0x79, 0xA2, 0x00, 0x03, 0x1D };

// Timezone TimeZone;

WiFiClient WifiNetwork;
MqttClient mqttClient(WifiNetwork);

// Status led Ticker
void tick() {
  //toggle state
  int MainLedState = digitalRead(STATUS_LED_1);  // get the current state of GPIO1 pin
  digitalWrite(STATUS_LED_1, !MainLedState);     // set pin to the opposite state
}

// Function to reverse elements of an array
void reverse(uint8_t arr[], int n){
	uint8_t aux[n];

	for (int i = 0; i < n; i++) {
		aux[n - 1 - i] = arr[i];
	}

	for (int i = 0; i < n; i++) {
		arr[i] = aux[i];
	}
}

TimeData MsToTimeData(unsigned long time){

  TimeData timeData =  (TimeData){0,0};
 
  timeData.seconds = time / 1000;
  timeData.minutes = timeData.seconds / 60;
  
  timeData.seconds %= 60;
  timeData.minutes %= 60;

  return timeData;
  
}

String MsToTimeString(unsigned long time){

  TimeData timeData = MsToTimeData(time);

  // creates base strings
  String StrMinutes = String(round(timeData.minutes), 0);
  String StrSeconds = String(round(timeData.seconds), 0);

  // removes spaces
  StrMinutes.trim();
  StrSeconds.trim();
	
  // leading zeroes
  StrMinutes = timeData.minutes < 10 ? "0" + StrMinutes : StrMinutes;
	StrSeconds = timeData.seconds < 10 ? "0" + StrSeconds : StrSeconds;

  String res = String(StrMinutes + ":" + StrSeconds); 

  return res;

}

void setup() {

  Sbegin(115200);

  // Setup status led
  pinMode(STATUS_LED_1, OUTPUT);
  digitalWrite(STATUS_LED_1, HIGH);

  // start ticker with 0.6 because we start in AP mode and try to connect
  StatusLedTicker.attach(0.2, tick);

  ReadSettings();

  SetupConfigMode(ObjGlobal_WifiManager);
  
  LoadConfig();
  
  SetupHardware();
  
  SetupMqtt();

  ReloadStatus();

  // first publish, to avoid too much delay
  PublishStatusMqtt();
  PublishAmbientDataMqtt();
  // // Setup web updater
  httpUpdater.setup(&ObjGlobal_HttpServer, SettingsData.update_path, SettingsData.update_username, SettingsData.update_password);

  SetupWebHandlers();

  // SetupTime();

}

int LastConnectionStatus = 99;

void loop() {

  events();

  ObjGlobal_HttpServer.handleClient();

  CheckStatusThread.check();

  BtnsThread.check();
  BlinkThread.check();

  PublishMqttStatusThread.check();
  
  PublishMqttAmbientThread.check();
  
  int connectionStatus = WiFi.status();

  // connection status has changed, and is not connected, reboot
  if(connectionStatus != LastConnectionStatus &&
     connectionStatus != 3){
    Sprint("Connection lost, code: ");
    Sprintln(connectionStatus);
    delay(5000);
  	ESP.reset();

  }
  
  LastConnectionStatus = connectionStatus;
}


// # switch:
// #   - platform: mqtt
// #     unique_id: water_sys_override
// #     name: "WaterSystemSensorOverride"
// #     # state_topic: "home/waterSystem/pumpState"
// #     state_topic: "home/waterSystem/statusData"
// #     value_template: "{{ value_json.sOverride }}"
// #     command_topic: "home/waterSystem/action"
// #     availability:
// #       - topic: "home/waterSystem/availability"
// #     payload_on: "{\"action\": \"setOverride\", \"overrideSensor\": true}"
// #     payload_off: "{\"action\": \"setOverride\", \"overrideSensor\": false}"
// #     state_on: "true"
// #     state_off: "false"
// #     optimistic: false
// #     qos: 0
// #     # retain: true