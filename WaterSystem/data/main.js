var debug = false;
$(document).ready(function(){

  	//start manage loader
	$(document).ajaxSend(function(event, jqxhr, settings) {
    //default loader used
    if(settings.dataSkiploader){
      return;
    }
		$('#generalLoader').fadeIn(150);
	});

	$(document).ajaxComplete(function(event, jqxhr, settings) {
    $('#generalLoader').fadeOut(300);			
	});
  //end manage loader
  
  $("#status_overrideSensor").change(function() {
    if(checkEventIgnore){
      return;
    }

    if($(this).is(':checked')){
      SetOverride(true);
    }
    else{
      SetOverride(false);
    }
  });

  $("#status_testMode").change(function() {
    if(checkEventIgnore){
      return;
    }

    if($(this).is(':checked')){
      SetTestMode(true);
    }
    else{
      SetTestMode(false);
    }
  });

  ReloadAll();

  setInterval(function(){
   GetStatus(false, true);
  }, 1000);

});

var checkEventIgnore = false;

var Paths = {
  osConfig: '/osConfig',
  osSystem: '/osSystem',
  osAction: '/osAction',
  osStatus: '/osStatus'
}

var GlobalConfigData = null

var GaugeCanvas = null

function ReloadAll(){
  GetConfig();
  GetStatus(true, false);
}

function pad(num, size) {
  var s = "000000000" + num;
  return s.substr(s.length-size);
}

function osSystem(action) {

  var dataJSON = {
    action: action
  }

  $.ajax({
    type: "POST",
    url: Paths.osSystem,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      location.reload();
    
    },
    error: function(jqXHR,error, errorThrown) { 
      alert("Operation failed"); 
      console.log("Something went wrong");
      
    }
  });

}

function GetConfig() {

  var dataJSON = {
    action: "getConfig"
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      RenderConfig(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");

      if(debug == true){
        var configDummy = {"code": "0", "response":{"autoWater":false,"pumpPin":13,"pumpPinInverted":false,"waterTime":60000,"waterLevelLow":0,"waterLevelHigh":1,"startHour":7,"startMinutes":0,"stopHour":7,"stopMinutes":3,"mqttServer":{"userName":"pi","key":"f4e7889@@@","location":"192.168.1.2","port":1883,"connAttempts":5}}};
        RenderConfig(configDummy.response);
      }

    }
  });
}

// edit creation
var arrSliders = [];

var pinArray = [
  {
    pin: 12,
    nodePin: "D6"
  }, 
  {
    pin: 13,
    nodePin: "D7"
  }, 
  {
    pin: 14,
    nodePin: "D5"
  },
  {
    pin: 15,
    nodePin: "D8"
  }, 
  {
    pin: 16,
    nodePin: "D0"
  }
]

function RenderConfig(configData){
  
  // saves configuration
  GlobalConfigData = configData;

  var pinContainer = $('#mainConfig_PumpPin').html('');
    
  for (let i = 0; i < pinArray.length; i++) {
    const element = pinArray[i];
    pinContainer.append('<option value="' + element['pin'] + '">' + element['pin'] + ' - ' + element['nodePin'] + '</option>');
  }
  pinContainer.val(configData.pumpPin);

  $('#mainConfig_PumpPinInverted').prop('checked', configData.pumpPinInverted );

  var waterDefTime = millisToMinutesAndSeconds(configData.waterTime);
  $('#mainConfig_WaterTime').val(waterDefTime.minutes + ':' + waterDefTime.seconds);
  $('#timed_timeToRun').val(waterDefTime.minutes + ':' + waterDefTime.seconds);
  
  $('#mainConfig_autoWater').prop('checked', configData.autoWater );
  
  $('#mainConfig_startTime').val(pad(configData.startHour, 2) + ':' + pad(configData.startMinutes, 2));
  $('#mainConfig_stopTime').val(pad(configData.stopHour, 2) + ':' + pad(configData.stopMinutes, 2));
  
  $('#waterLevelConfig_low').val(configData.waterLevelLow);
  $('#waterLevelConfig_high').val(configData.waterLevelHigh);

  // delete gauge, to allow reload
  GaugeCanvas = null;

  $('#mqttServerConfig_userName').val(configData.mqttServer.userName);
  $('#mqttServerConfig_key').val(configData.mqttServer.key);
  $('#mqttServerConfig_location').val(configData.mqttServer.location);
  $('#mqttServerConfig_port').val(configData.mqttServer.port);
  $('#mqttServerConfig_connAttempts').val(configData.mqttServer.connAttempts);

// mqttServerConfig_connCount
}

function SaveConfigData(){

  var pumpPin = $('#mainConfig_PumpPin').val();
  var pumpPinInverted = $('#mainConfig_PumpPinInverted').prop("checked");

  var timeObj = splitTimeString($('#mainConfig_WaterTime').val());

  var WaterDefaultTime = minutesAndSecondsToMilliseconds(timeObj.minutes, timeObj.seconds);
  
  var autoWater = $('#mainConfig_autoWater').prop('checked');

  let startTime = $('#mainConfig_startTime').val();
  let startArr = startTime.split(':');
  let startHour = parseInt(startArr[0], 10);
  let startMinutes = parseInt(startArr[1], 10);
  
  let stopTime = $('#mainConfig_stopTime').val();
  let stopArr = stopTime.split(':');
  let stopHour = parseInt(stopArr[0], 10);
  let stopMinutes = parseInt(stopArr[1], 10);

  let waterLevelLow = $('#waterLevelConfig_low').val();
  let waterLevelHigh = $('#waterLevelConfig_high').val();

  let mqttUserName = $('#mqttServerConfig_userName').val();
  let mqttKey = $('#mqttServerConfig_key').val();
  let mqttLocation = $('#mqttServerConfig_location').val();
  let mqttPort = $('#mqttServerConfig_port').val();
  let mqttConnAttempts = $('#mqttServerConfig_connAttempts').val();

  // get data
  var ConfigData = {
    pumpPin: pumpPin,
    pumpPinInverted: pumpPinInverted,
    waterTime: WaterDefaultTime,
    autoWater: autoWater,
    startHour: startHour,
    startMinutes: startMinutes,
    stopHour: stopHour, 
    stopMinutes: stopMinutes,
    waterLevelLow: waterLevelLow,
    waterLevelHigh: waterLevelHigh,
    mqttServer:{
      userName: mqttUserName,
      key: mqttKey,
      location: mqttLocation,
      port: mqttPort,
      connAttempts: mqttConnAttempts
    }
  };

  SaveConfigDataWebCall(ConfigData);
}

function SaveConfigDataWebCall(ConfigData){

  var dataJSON = {
    action: 'saveConfig',
    value: ConfigData
  }

  $.ajax({
    type: "POST",
    url: Paths.osConfig,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      
      // reload interface
      RenderConfig(msg.response);
      
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

var globalSaveLimit = -1;
function SetCurrentLimit(limit){
  // sets the variable, so that at the next statusData income, the value will be saved
  $('#generalLoader').fadeIn(150);
  globalSaveLimit = limit;

}

function SaveLimit(statusData){

  let element;

  if(globalSaveLimit == 0){
    element = $('#waterLevelConfig_low');
  }
  else if(globalSaveLimit == 1){
    element = $('#waterLevelConfig_high');
  }
  else{
    return;
  }

  // restore flag
  globalSaveLimit = -1;

  // saves the current value in the html element
  element.val(statusData.waterLevelData.raw);
  
  // call che classic config save
  SaveConfigData();
}

function TogglePump(action){

  // get override sensor
  var overrideSensor = $('#status_overrideSensor').prop("checked");

  var dataJSON = {
    action: 'pumpOn',
    value: {
      overrideSensor: overrideSensor
    }
  }

  if(action == false){
    dataJSON.action = 'pumpOff';
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

function splitTimeString(timeString, separator = ':'){

  timeString = timeString.split(separator);

  let minutes = parseInt(timeString[0], 10);

  if(timeString[1] == undefined || timeString[1] == null){
    timeString[1] = 0;
  }

  let seconds = parseInt(timeString[1], 10);

  var returnObj = {
    minutes: minutes,
    seconds: seconds  
  }

  return  returnObj;
}

function minutesAndSecondsToMilliseconds(min,sec){
  return((min*60+sec)*1000);
}

function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  
  var returnObj = {
    minutes: (minutes < 10 ? '0' : '') + minutes,
    seconds: (seconds < 10 ? '0' : '') + seconds  
  }

  return  returnObj;
}

function TimedStart(action){

  var overrideSensor = $('#status_overrideSensor').prop("checked");
  
  var timeObj = splitTimeString($('#timed_timeToRun').val());
  var millisecondsTorun = minutesAndSecondsToMilliseconds(timeObj.minutes, timeObj.seconds);
  
  var dataJSON = {
    action: 'timedStart',
    value: {
      overrideSensor: overrideSensor,
      timeToRun: millisecondsTorun
    } 
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

// this will be called every second
function SetOverride(value) {

  var dataJSON = {
    action: 'overrideSensors',
    value: {
      overrideSensor: value
    }
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
    }
  });
}

function SetTestMode(value) {

  var dataJSON = {
    action: 'testMode',
    value: {
      testMode: value
    }
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: false,
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
    }
  });
}



// function ToggleFan(action){

//   // get override sensor
//   // var overrideSensor = $('#status_overrideSensor').prop("checked");

//   var dataJSON = {
//     action: 'fanOn',
//   }

//   if(action == false){
//     dataJSON.action = 'fanOff';
//   }

//   $.ajax({
//     type: "POST",
//     url: Paths.osAction,
//     data: JSON.stringify(dataJSON),
//     dataType: "json",
//     success: function(msg) {

//       //RenderStatus(msg.response);
    
//     },
//     error: function(jqXHR,error, errorThrown) {  
//       console.log("Something went wrong");
      
//     }
//   });

// }

// this will be called every second
function GetStatus(reload, skipLoader) {

  var dataJSON = {
    action: 'GetStatus'
  }

  if(reload == true){
    dataJSON.action = 'GetReloadStatus';
  }

  $.ajax({
    type: "POST",
    url: Paths.osStatus,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    dataSkiploader: skipLoader,
    success: function(msg) {

      // save the current water limit
      if(globalSaveLimit != -1){
        SaveLimit(msg.response);
      }

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
      if(debug == true){
        statusDummy = {"code": "0", "response":{"machineState":0,"isPumpActive":false,"isPumpFlood":false,"sensorOverride":false,"elapsedTime":0,"timedStart":false,"ambientData":{"temperature":0,"humidity":0,"pressure":0,"altitude":0,"psuTemperature":-127},"waterLevelData":{"raw":0.13916,"raw_c":0.13916,"level":14,"isLow":0},"mqttStatus":{"connected":1,"state":0,"connCount":0}}};
        RenderStatus(statusDummy.response);
      }
    }
  });
}

// this will be called every second
function RenderStatus(statusData){
  
  // sensor and Pump status
  var objPumpStatus = $('#status_isPumpOn');
  var sensorStatus = $('#status_sensorTriggered');

  objPumpStatus.val(statusData.isPumpActive);
  sensorStatus.val(statusData.waterLevelData.isLow);
  
  // disable onchange event
  checkEventIgnore = true;
  $('#status_overrideSensor').prop("checked", statusData.sensorOverride);
  $('#status_testMode').prop("checked", statusData.testMode);
  checkEventIgnore = false;

  var elapsedTime = millisToMinutesAndSeconds(statusData.elapsedTime);
  
  $('#timed_elapsedTime').val(elapsedTime.minutes + ':' + elapsedTime.seconds);
  $('#manual_elapsedTime').val(elapsedTime.minutes + ':' + elapsedTime.seconds);
 
  var waterTime = millisToMinutesAndSeconds(statusData.waterTime);
  
  $('#timed_settedTimeToRun').val(waterTime.minutes + ':' + waterTime.seconds);

  // enable/disable all buttons
  var overrideSensor = statusData.sensorOverride;//$('#status_overrideSensor').prop("checked");
  
  UpdateWaterGauge(statusData.waterLevelData.level);

  var txtTimed = $('#timed_timeToRun');

  // to change, add error variable to backend
  if((statusData.machineState == 0 || statusData.machineState == 1) || overrideSensor){
    // good state
    HidePills('.status_statusPillError');
    $('.controlBtn').prop('disabled', false);
  
    var btnManualOn = $('#manual_onButton');
    var btnManualOff = $('#manual_offButton');
    var btntimedOn = $('#timed_onButton');

    if(!statusData.isPumpActive){
      HidePills('.status_statusPillOn');
      ShowPills('.status_statusPillReady');
      btnManualOn.prop('disabled', false);
      btntimedOn.prop('disabled', false);
      btnManualOff.prop('disabled', true);
      txtTimed.prop('disabled', false);
    }
    else{
      HidePills('.status_statusPillReady');
      ShowPills('.status_statusPillOn');
      btnManualOn.prop('disabled', true);
      btntimedOn.prop('disabled', true);
      btnManualOff.prop('disabled', false);
      txtTimed.prop('disabled', true);
    }
  }
  else{
    // error state, 
    $('.status_statusPillError').html(ConvertMachineStateToH(statusData.machineState));
      
    HidePills('.status_statusPillOn, .status_statusPillReady');
    ShowPills('.status_statusPillError');
    $('.controlBtn').prop('disabled', true);
    txtTimed.prop('disabled', true);

  }
  
  $('#ambientData_temperature').html(Math.round(statusData.ambientData.temperature));
  $('#ambientData_humidity').html(Math.round(statusData.ambientData.humidity));
  $('#ambientData_pressure').html(Math.round(statusData.ambientData.pressure));
  $('#ambientData_psuTemperature').html(Math.round(statusData.ambientData.psuTemperature));

  // connecting
  if(statusData.mqttStatus.connected == 2){
    HidePills('.mqttServerConfig_connected');
    HidePills('.mqttServerConfig_disconnected');
    ShowPills('.mqttServerConfig_connecting');
    $('#mainConfig_mqttConnect').prop('disabled', true);

    $('#mqttServerConfig_connCount').html(statusData.mqttStatus.connCount);
    ShowPills('#mqttServerConfig_connecting');

    return;
  }

  // mqtt disconnected
  if(statusData.mqttStatus.connected == 0){
    HidePills('.mqttServerConfig_connected');
    HidePills('.mqttServerConfig_connecting');
    ShowPills('.mqttServerConfig_disconnected');
    $('#mainConfig_mqttConnect').prop('disabled', false);
    HidePills('#mqttServerConfig_connCount');
  }
  else{
    HidePills('.mqttServerConfig_disconnected');
    HidePills('.mqttServerConfig_connecting');
    ShowPills('.mqttServerConfig_connected');
    $('#mainConfig_mqttConnect').prop('disabled', true);
    HidePills('#mqttServerConfig_connCount');
  }

}

function GetGaugeLabels(divisions){
  
  var resArray = [];
  var increment = 100 / (divisions);
  var current = 0;

  for (let i = 0; i <= divisions; i++) {
    resArray[i] = current;
    current = current + increment;
  }
  
  return resArray;

}

function GetGaugeZones(){
  
  var resObject = [];
  var lowLimit = 10;
  var midLimit = 30;
  var maxLevel = 100;
  
  resObject.push({strokeStyle: "#F03E3E", min: 0, max: lowLimit});
  resObject.push({strokeStyle: "#FFDD00", min: lowLimit, max: midLimit});
  resObject.push({strokeStyle: "#30B32D", min: midLimit, max: maxLevel});

  return resObject;
}

function UpdateWaterGauge(waterLevel){

  if (waterLevel == 255) {
    waterLevel = 0;
  }

  if(GaugeCanvas != null){
    GaugeCanvas.set(waterLevel); // set actual value
    GaugeCanvas.setTextField(document.getElementById("waterLevelGaugeText"));
    return;
  }

  var opts = {
    angle: -0.2, // The span of the gauge arc
    lineWidth: 0.2, // The line thickness
    radiusScale: 1, // Relative radius
    // fontSize:53,
    pointer: {
      length: 0.63, // // Relative to gauge radius
      strokeWidth: 0.038, // The thickness
      color: '#000000' // Fill color
    },
    limitMax: false,     // If false, max value increases automatically if value > maxValue
    limitMin: false,     // If true, the min value of the gauge will be fixed
    colorStart: '#6F6EA0',   // Colors
    colorStop: '#C0C0DB',    // just experiment with them
    strokeColor: '#EEEEEE',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    
    staticLabels: {
      font: "10px sans-serif",  // Specifies font
      labels: GetGaugeLabels(10),  // Print labels at these values
      color: "#000000",  // Optional: Label text color
      fractionDigits: 0  // Optional: Numerical precision. 0=round off.
    },
    generateGradient: true,
    staticZones: GetGaugeZones(),
    renderTicks: {
      divisions: 10,
      divWidth: 1.1,
      divLength: 0.7,
      divColor: '#333333',
      subDivisions: 0,
      subLength: 0.5,
      subWidth: 0.6,
      subColor: '#666666'
    }
  };

  var target = document.getElementById('waterLevelGaugeCanvas'); // your canvas element
  GaugeCanvas = new Gauge(target).setOptions(opts); // create sexy gauge!
  GaugeCanvas.setTextField(document.getElementById("waterLevelGaugeText"));
  GaugeCanvas.maxValue = 100; // set max gauge value
  GaugeCanvas.setMinValue(0);  // Prefer setter over gauge.minValue = 0
  GaugeCanvas.animationSpeed = 15; // animation speed lower is faster
  GaugeCanvas.set(UpdateWaterGauge); // set actual value

}

function ShowPills(pills){
  $(pills).show();
}

function HidePills(pills){
  $(pills).hide();
}

function ConvertMachineStateToH(status){

  switch (status) {
    case 0:
      return 'Ready';      
      break;
    case 1:
      return 'Running';      
      break;
    case 2:
      return 'Low Water';      
      break;
    case 3:
      return 'Water Sensor Error';      
      break;
    case 4:
      return 'Pump is Flood';      
      break;
    case 99:
      return 'Error';      
      break;

    default:
      return null;
    break;
  }

}

function ConvertMachineState(status){

  switch (status) {
    case 0:
      return 'STATE_IDLE';      
      break;
    case 1:
      return 'STATE_PUMP_ON';      
      break;
    case 2:
      return 'STATE_ERROR_LOW_WATER';      
      break;
    case 3:
      return 'STATE_ERROR_WATER_SENSOR_ERROR';      
      break;
    case 4:
      return 'STATE_ERROR_PUMP_FLOOD';      
      break;
    case 99:
      return 'STATE_ERROR';      
      break;

    default:
      return null;
    break;
  }

}

function ConnectMqtt(action){

  var dataJSON = {
    action: 'mqttConnect'
  }

  $.ajax({
    type: "POST",
    url: Paths.osAction,
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      RenderStatus(msg.response);
    
    },
    error: function(jqXHR,error, errorThrown) {  
      console.log("Something went wrong");
      
    }
  });

}

