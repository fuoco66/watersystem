void SetupMqtt(){

  // sets is milliseconds
  // time to keep the connection open even if the client doesn't sends any data
  mqttClient.setKeepAliveInterval(GlobalKeepAlive);

  mqttClient.setId(SettingsData.host);
  mqttClient.setUsernamePassword(ConfigurationData.mqttServer.userName, ConfigurationData.mqttServer.key);
  mqttClient.setConnectionTimeout(1000);
  // mqttClient.setCleanSession(true);

  // resets connection count
  StatusData.mqttStatus.connCount = 0;

  if(ConnectMqtt() != 0){ // tries to connect
    return;
  }
}

void CheckMqttStatus(){

	mqttClient.poll();

  // client connected, connected = 1
	if(mqttClient.connected()){
    return;
  }
  // client Disconnected

  // check if too many attempts
  if(StatusData.mqttStatus.connCount >= ConfigurationData.mqttServer.connAttempts){

    long int timePassed = millis() - StatusData.mqttStatus.lastConnection;
    if(timePassed < MQTT_RECONNECT_TIME){
      StatusData.mqttStatus.connected = 0;
      // get last connection error
      StatusData.mqttStatus.state = mqttClient.connectError();
      return;
    }

    // if enough time has passed, resets connection count
    Sprintln("More than 60 seconds, resets connection counter");
    StatusData.mqttStatus.connCount = 0;
  }

  // sets status to connecting
  StatusData.mqttStatus.connected = 2;

  Sprint("Attempting to reconnect to the MQTT broker: ");

  // increment connection count
  StatusData.mqttStatus.connCount = StatusData.mqttStatus.connCount + 1;
  // saves current connection time
  StatusData.mqttStatus.lastConnection = millis();
  

  // when 0, reconnected
  if(ConnectMqtt() != 0){ // try reconnecting
    return;
  }

  StatusData.mqttStatus.connected = 1;
  StatusData.mqttStatus.state = 0;
  Sprint("Client reconnected!");
  Sprintln();

}

int ConnectMqtt(){

  Sprint("Attempt to connect to the MQTT broker: ");
  Sprintln(ConfigurationData.mqttServer.location);
  
  SetupLastWill();

  if (!mqttClient.connect(ConfigurationData.mqttServer.location, ConfigurationData.mqttServer.port)) {
    // not connected

    int connectionError = mqttClient.connectError();
    
    Sprint("MQTT connection failed! Error code = ");
    Sprintln(connectionError);

    StatusData.mqttStatus.state = connectionError;
    
    return connectionError;
  }

  Sprint("Client connected!");
  Sprintln();
  // connected  
  StatusData.mqttStatus.connected = 1;
  StatusData.mqttStatus.state = 0;

  // resets connection count
  StatusData.mqttStatus.connCount = 0;
   
  PublishMessage(GlobalAvailableTopic, "online");
  
  SubscribeActionTopicMqtt();

  return 0;
}

void SubscribeActionTopicMqtt(){

  mqttClient.unsubscribe(GlobalActionTopic);

  mqttClient.onMessage(onMqttAction);

  Sprint("Subscribing to topic: ");
  Sprintln(GlobalActionTopic);
  Sprintln();

  // subscribe to a topic
  mqttClient.subscribe(GlobalActionTopic);
}

void onMqttAction(int messageSize) {

  String arg;
  while (mqttClient.available()) {
    arg += (char)mqttClient.read();
  }

  // parse argument
  DynamicJsonDocument objPayload(215);
  DeserializationError jsonError = deserializeJson(objPayload, arg);

  if(objPayload["action"] == "pumpOn"){

    bool overrideSet = objPayload.containsKey("overrideSensor");

    if(overrideSet == true){
      StatusData.sensorOverride = objPayload["overrideSensor"].as<bool>();
    }

    PumpON();
    // {"action": "pumpOn",
    //  "overrideSensor": false
    // }
  }
  else if(objPayload["action"] == "pumpOff"){
    PumpOFF();
    // {"action": "pumpOff"}
  }
  else if(objPayload["action"] == "setOverride"){
    StatusData.sensorOverride = objPayload["value"].as<bool>();

  }
  else if(objPayload["action"] == "timedStart"){
    
    bool overrideSet = objPayload.containsKey("overrideSensor");
    if(overrideSet == true){
      StatusData.sensorOverride = objPayload["overrideSensor"].as<bool>();
    }
    
    float timeToRun = ConfigurationData.waterTime;

    bool timeSet = objPayload.containsKey("timeToRun");
    if(timeSet == true){
      timeToRun = objPayload["timeToRun"].as<float>();
      timeToRun = (timeToRun*60)*1000;
    }

    StatusData.waterTime = timeToRun;

    TimedStart();
    // { "action": "timedStart",
    //  "overrideSensor": false
    //   "timeToRun": 3600
    // }
  }
  else if(objPayload["action"] == "reboot"){
    
    Reboot();
   // { "action": "reboot"
    // }
  }

}

void PublishStatusMqtt(){
  
  // client connected, connected = 1
	if(!mqttClient.connected()){
    // disconnected, skip
    return;
  }

  // increments the status renew timer
  StatusData.mqttStatus.statusTimer = StatusData.mqttStatus.statusTimer + 1000;

  bool forceStatus = false;
  // time passed force the update
  if(StatusData.mqttStatus.statusTimer >= GlobalStatusTimer){
    forceStatus = true;
    StatusData.mqttStatus.statusTimer = 0;
  }

  // sends data only if something has changed
  if(MqttCache.machineState == StatusData.machineState &&
     MqttCache.sensorOverride == StatusData.sensorOverride &&
     MqttCache.testMode == StatusData.testMode &&
     MqttCache.isPumpActive == StatusData.isPumpActive &&
     MqttCache.elapsedTime == StatusData.elapsedTime &&
     MqttCache.waterLevel == StatusData.waterLevelData.level &&
     !StatusData.isPumpActive && !forceStatus){
    return;
  }

  // updates cache
  MqttCache.machineState = StatusData.machineState;
  MqttCache.sensorOverride = StatusData.sensorOverride;
  MqttCache.testMode = StatusData.testMode;
  MqttCache.isPumpActive = StatusData.isPumpActive;
  MqttCache.elapsedTime = StatusData.elapsedTime;
  MqttCache.waterLevel = StatusData.waterLevelData.level;

  DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();

	root["state"] = StatusData.machineState;
	root["sOverride"] = StatusData.sensorOverride;
	root["tMode"] = StatusData.testMode;

  root["eTime"] = MsToTimeString(StatusData.elapsedTime);

	root["wTime"] =  MsToTimeString(StatusData.waterTime);

	root["pumpOn"] = StatusData.isPumpActive;
  
  root["wLevel"] = StatusData.waterLevelData.level == 255 ? 0 : StatusData.waterLevelData.level;

  String webResponse;
  serializeJson(root, webResponse);

  //renew available topic
  PublishMessage(GlobalAvailableTopic, "online");
  PublishMessage(GlobalStatusTopic, webResponse);

}

void PublishAmbientDataMqtt(){
  
  // client connected, connected = 1
	if(!mqttClient.connected()){
    // disconnected, skip
    return;
  }

  uint8_t tmpTemperature = round(StatusData.ambientData.temperature);
  uint8_t tmpHumidity = round(StatusData.ambientData.humidity);
  uint16_t tmpPressure = round(StatusData.ambientData.pressure);
  uint16_t tmpAltitude = round(StatusData.ambientData.altitude);
  uint8_t tmpPsuTemperature = round(StatusData.ambientData.psuTemperature);

  // sends data only if something has changed
  // disabled because it's used to determine if the client is online or not
  // if(MqttCache.temperature == tmpTemperature &&
  //    MqttCache.humidity == tmpHumidity &&
  //    MqttCache.pressure == tmpPressure &&
  //    MqttCache.altitude == tmpAltitude &&
  //    MqttCache.psuTemperature == tmpPsuTemperature){
  //   return;
  // }

  // updates cache
  MqttCache.temperature = tmpTemperature;
  MqttCache.humidity = tmpHumidity;
  MqttCache.pressure = tmpPressure;
  MqttCache.altitude = tmpAltitude;
  MqttCache.psuTemperature = tmpPsuTemperature;

  DynamicJsonDocument jsonDoc(1024);
	JsonObject root = jsonDoc.to<JsonObject>();

	root["tmp"] = tmpTemperature;
  root["hum"] = tmpHumidity;
  root["press"] = tmpPressure;
  root["alt"] = tmpAltitude;
  root["psuTmp"] = tmpPsuTemperature;

  String webResponse;
  serializeJson(root, webResponse);

  PublishMessage(GlobalAmbientDataTopic,webResponse);

}

bool PublishMessage(String topic, String payLoad){
  
  char charTopic[200], charPayload[200];
  topic.toCharArray(charTopic, 200);
  payLoad.toCharArray(charPayload, 200);
  PublishMessage(charTopic, charPayload);

}

bool PublishMessage(const char* topic, const char* payLoad){

  mqttClient.beginMessage(topic);
  mqttClient.print(payLoad);
  mqttClient.endMessage();

}

bool SetupLastWill(){
  
  const char payLoad[]  = "offline";
  
  mqttClient.beginWill(GlobalAvailableTopic, true, 0);
  mqttClient.print(payLoad);
  mqttClient.endWill();

}