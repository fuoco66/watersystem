void PumpON(){

	if( (StatusData.waterLevelData.isLow || StatusData.isPumpFlood) && !StatusData.sensorOverride){
		return;
	}
	
	StatusData.startTime = millis();

	// if in test mode, doesn't turn the pin on, but changes the status
	if(StatusData.testMode){
		// force reload
		ReloadStatus();
		// force the status variable
		StatusData.isPumpActive = true;
		return;
	}

	if(!ConfigurationData.pumpPinInverted){
	  digitalWrite(ConfigurationData.pumpPin, HIGH);
	}
	else{
	  digitalWrite(ConfigurationData.pumpPin, LOW);
	}

	// force reload
	ReloadStatus();
}

void PumpOFF(){

	if(StatusData.testMode){
		// force the status variable
		StatusData.isPumpActive = false;
	}
	
	if(!ConfigurationData.pumpPinInverted){
	  digitalWrite(ConfigurationData.pumpPin, LOW);
	}
	else{
	  digitalWrite(ConfigurationData.pumpPin, HIGH);
	}

	StatusData.timedStart = false;
	StatusData.startTime = 0;
	StatusData.waterTime = 0;
	// StatusData.elapsedTime = 0;

	// force reload
	ReloadStatus();
}

void TimedStart(){

	StatusData.timedStart = true;
	PumpON();
		
}

void ToggleTestModeOn(bool status){

	StatusData.testMode = status;

	if(status){
		// turns the punp off
		PumpOFF();
	}

}

// void FanON(){
	
// 	PortExpander.WritePin(FAN_PIN, LOW);

// 	// force reload
// 	ReloadStatus();
// }

// void FanOFF(){

// 	PortExpander.WritePin(FAN_PIN, HIGH);

// }

uint8_t GetPressedButton(){

	// get vurrent time
	unsigned long currentMillis = millis();
	
	// if debouncing input is ignored
	if(currentMillis - StatusData.lastDebounceTime <= 500) {
		return 0;
	}

	// update time
	StatusData.lastDebounceTime = currentMillis;

	// get analog value
	uint16_t selectPinValue = analogRead(A0);
	uint8_t btnPressed;

	// get button id, from analog value
  if(selectPinValue <= ZERO_LIMIT) {
    btnPressed = 0;
  }
  else if(selectPinValue <= BTN_1_LIMIT) {
		btnPressed = 1;
	}
  else if(selectPinValue <= BTN_2_LIMIT) {
		btnPressed = 2;
	}
	else{
    btnPressed = 0;
  }

	// if button is the same as before, do nothing
	if(btnPressed == StatusData.lastPressedButton){
		return 0;
	}

	StatusData.lastPressedButton = btnPressed;

	return btnPressed;

}

void CheckBtns(){
	
	if(StatusData.machineState != STATE_IDLE && StatusData.machineState != STATE_PUMP_ON){
		return;
	}

	uint8_t pressedButton = GetPressedButton();
  
	if(pressedButton == 0) {
		return;
	}
  else if(pressedButton == 1) {
		SetBlink(2);
  	PumpOFF();
	}
  else if(pressedButton == 2) {
		SetBlink(2);
		PumpON();  
	}
}

void SetBlink(uint8_t blinkCount){
	// doubles because the off-on counts as two times
	StatusData.blinkCount = blinkCount*2;
	StatusData.lastBlink = 0;
}

void BlinkButtonLed(){

	// skip because led is off
  if(StatusData.machineState != STATE_IDLE && StatusData.machineState != STATE_PUMP_ON){
    StatusData.blinkCount = 0;
  	return;
	}

  if (StatusData.blinkCount > 0) {

    unsigned long currentMillis = millis();
    
    if (currentMillis - StatusData.lastBlink >= BTN_LED_BLINK) {
      // save the last time you blinked the LED
      StatusData.lastBlink = currentMillis;

      GlobalBtnLedStateForBlink = !GlobalBtnLedStateForBlink;
      PortExpander.WritePin(BTN_LED, GlobalBtnLedStateForBlink);
      StatusData.blinkCount = StatusData.blinkCount -1;
    }
    
  }

}