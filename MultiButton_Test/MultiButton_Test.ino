#define MULTI_BTN_PIN A0

#define ZERO_LIMIT 220
#define BTN_1_LIMIT 600
#define BTN_2_LIMIT 990

// ADC_MODE(ADC_VCC);

void setup() {
  Serial.begin(115200);

  pinMode (A0, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  CheckState();
  
  delay(500);
}

uint16_t CheckState(){
   
  // read the input on analog pin:
  uint16_t selectPinValue = analogRead(A0);
  
  Serial.print("Analog Pin:  ");
  Serial.println(selectPinValue);

  if(selectPinValue <= ZERO_LIMIT) {
    Serial.println("No Pression");
  }
  else if(selectPinValue <= BTN_1_LIMIT) {
    Serial.println("Btn 1 Pressed");
  }
  else if(selectPinValue <= BTN_2_LIMIT) {
    Serial.println("Btn 2 Pressed");
  }else{
    Serial.println("Undefined");
  }

  return selectPinValue;
}