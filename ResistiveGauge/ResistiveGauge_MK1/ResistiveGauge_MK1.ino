/*

    Ultrasonic sensor Pins:
        VCC: +5VDC
        Trig : Trigger (INPUT) - Pin11
        Echo: Echo (OUTPUT) - Pin 12
        GND: GND
 */
#include <Wire.h>
#define I2C_SLAVE_ADDRESS 0x08 // Address of the slave

#define OHM_PIN A3

int raw = 0;
int Vin = 5;
float Vout = 0;
float R1 = 1000;
float R2 = 0;
float buffer = 0;

char command;

unsigned long lastCapture = 0; 

enum {
  CMD_ID = 1,
  CMD_READ  = 2,
};

void setup() {

  // Start the I2C Bus as Slave on address 9
  Wire.begin(I2C_SLAVE_ADDRESS); 
  
  // Attach a function to trigger when something is received.
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  
}

void receiveEvent (int howMany){
  // ditch = true;

  command = Wire.read ();  // remember command for when we get request
  // Serial.println("rec");

} // end of receiveEvent


void requestEvent() {

  // ditch = true;

  switch (command) {
    case CMD_ID:      Wire.write (0x55); break;   // send our ID 
    case CMD_READ:
      // Serial.println("req");

      byte SlaveSend = R2;
      Wire.write(SlaveSend);
      break;
    
  }  // end of switch
}

void loop() {

  // reads every seconds
  if((millis() - lastCapture) < 100){
    return;
  }

  raw = analogRead(OHM_PIN);
  if(raw){
    buffer = raw * Vin;
    Vout = (buffer)/1024.0;
    buffer = (Vin/Vout) - 1;
    R2 = R1 * buffer;
    // Serial.print("Vout: ");
    // Serial.println(Vout);
    // Serial.print("R2: ");
    // Serial.println(R2);
    // delay(1000);
  }
  else{
    R2 = 0;
  }
  lastCapture = millis();


  // int tmpDistance = ultrasonic.read(); 

  // // if interrupt breaks the capture, the result is discarted. Next reading will be immediate
  // if(ditch == true){
  //   Serial.println("ditched");
  //   lastCapture = millis();
  //   ditch = false;
  //   return;
  // }

  // // corrects
  // distance = tmpDistance + DIST_OFFSET;
  // Serial.print("Distance in CM: ");
  // Serial.println(distance);
  // lastCapture = millis();
}


    
