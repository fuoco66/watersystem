#include <Wire.h>
#include "ExpanderInterface.h"

#define ENDSW_PIN_COUNT 11 //Start Switch input

#define PUMP_FLOOD_PIN 7
#define WATER_LEVEL_PIN_0 1
#define WATER_LEVEL_PIN_1 2
#define WATER_LEVEL_PIN_2 0
#define WATER_LEVEL_PIN_3 3
#define WATER_LEVEL_PIN_4 6
#define WATER_LEVEL_PIN_5 4
#define WATER_LEVEL_PIN_6 5

#define RGB_0 12
#define RGB_1 13
#define RGB_2 14

// Arduino pins
#define EXP_PIN_INT 14 // Interrupt on this Arduino Uno pin.
#define EXP_ADDR 0 // Expander address

ExpanderInterface endStopInterface;

void setup(){
  Serial.begin(115200);
  pinMode(EXP_PIN_INT, INPUT);
  ExpanderInterface::ExpanderPin arrSwitch[11];
  arrSwitch[0] = endStopInterface.CreateInputPin(PUMP_FLOOD_PIN);
  arrSwitch[1] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_1);
  arrSwitch[2] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_2);
  arrSwitch[3] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_3);
  arrSwitch[4] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_4);
  arrSwitch[5] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_5);
  arrSwitch[6] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_6);
  arrSwitch[7] = endStopInterface.CreateInputPin(WATER_LEVEL_PIN_0);
  
  arrSwitch[8] = endStopInterface.CreateOutputPin(RGB_0, LOW);
  arrSwitch[9] = endStopInterface.CreateOutputPin(RGB_1, LOW);
  arrSwitch[10] = endStopInterface.CreateOutputPin(RGB_2, LOW);

  endStopInterface.begin(arrSwitch, ENDSW_PIN_COUNT, EXP_ADDR, EXP_PIN_INT);
}

void loop(){

  uint8_t res[8];
  
  endStopInterface.ReadPort(0, res);

  Serial.print("chached value: ");
  for (int i = 0; i < 8; i++)
  {
    Serial.print(i);
    Serial.print("-");
  }
  Serial.println();

  Serial.print("chached value: ");

  for (int i = 0; i < 8; i++)
  {
    Serial.print(res[i]);
    Serial.print("-");
  }

  // // Serial.print(endStopInterface.ReadPin(8));
  Serial.println(" ");

  Serial.println("HIGH");
  endStopInterface.WritePin(RGB_0, HIGH);
  
  delay(1000);
  
  Serial.println("LOW");
  endStopInterface.WritePin(RGB_0, LOW);
  delay(1000);

}
